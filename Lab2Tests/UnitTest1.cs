﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

// use the Lab2Lib library created in the Lab2Lib project.
using Lab2Lib;

// This lab introduces LinkedLists, Bags, Queues, and Stacks.
// The resulting console application should read arithmetic expressions
// from the console and print the result of the evaluations.  To
// receive full credit, you must implement the Bag, Queue and Stack
// classes using LinkedLists and use the two-stack evaluation function 
// described in Sedgewick section 1.3.

namespace Lab2Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void createVector()
        {
            Vector<int> myVector = new Vector<int>();
            Assert.IsTrue(myVector.IsEmpty);
        }

        //[TestMethod]
        //public void addToVector()
        //{
        //    Vector<int> myVector = new Vector<int>();
        //    myVector.Add(1);
        //    myVector.Add(2);
        //    myVector.Add(3);
        //    Assert.AreEqual(1, myVector[0]);
        //    Assert.AreEqual(2, myVector[1]);
        //    Assert.AreEqual(3, myVector[2]);
        //    Assert.AreEqual(3, myVector.Count);
        //}

        //[TestMethod]
        //You will need to implement the IEnumerable<Type> interface
        // in the Vector class.
        //public void implementIEnumerable()
        //{
        //    Vector<int> myVector = new Vector<int>();
        //    myVector.Add(1);
        //    myVector.Add(2);
        //    myVector.Add(3);
        //    int result = 1;
        //    foreach (int x in myVector)
        //    {
        //        Assert.AreEqual(result++, x);
        //    }
        //}

        //[TestMethod]
        //public void createNode()
        //{
        //    Node<int> testNode = new Node<int>(10, null);
        //    Assert.AreEqual(10, testNode.Item);
        //}

        //[TestMethod]
        //public void createLinkedList()
        //{
        //    Node<int> list = new Node<int>(3, null);
        //    list = list.Add(2);
        //    list = list.Add(1);
        //    Assert.AreEqual(1, list.Item);
        //    Assert.AreEqual(2, list.Next.Item);
        //    Assert.AreEqual(3, list.Next.Next.Item);
        //    Assert.IsNull(list.Next.Next.Next);
        //}

        //[TestMethod]
        //public void enumerateList()
        //{
        //    Node<int> list = new Node<int>(3, null);
        //    list = list.Add(2);
        //    list = list.Add(1);
        //    List<int> results = new List<int>();
        //    foreach (int item in list)
        //    {
        //        results.Add(item);
        //    }
        //    Assert.AreEqual(1, results[0]);
        //    Assert.AreEqual(2, results[1]);
        //    Assert.AreEqual(3, results[2]);
        //}

        //[TestMethod]
        //public void createBag()
        //{
        //    Bag<string> myBag = new Bag<string>();
        //    Assert.IsTrue(myBag.IsEmpty);
        //    Assert.AreEqual(0, myBag.Count);
        //}

        //[TestMethod]
        //public void addToBag()
        //{
        //    Bag<string> myBag = new Bag<string>();
        //    myBag.Add("abc");
        //    myBag.Add("def");
        //    myBag.Add("ghijk");
        //    Assert.AreEqual(3, myBag.Count);
        //}

        //[TestMethod]
        //public void iterateOverBag()
        //{
        //    Bag<int> myBag = new Bag<int>();
        //    myBag.Add(10);
        //    myBag.Add(100);
        //    myBag.Add(1000);

        //    int sumOfBag = 0;
        //    foreach (int nextInt in myBag)
        //    {
        //        sumOfBag += nextInt;
        //    }

        //    Assert.AreEqual(1110, sumOfBag);
        //}

        //[TestMethod]
        //public void createStack()
        //{
        //    Lab2Lib.Stack<string> myStack = new Lab2Lib.Stack<string>();
        //    Assert.IsTrue(myStack.IsEmpty);
        //}

        //[TestMethod]
        //public void pushStack()
        //{
        //    Lab2Lib.Stack<string> myStack = new Lab2Lib.Stack<string>();
        //    myStack.Push("first");
        //    myStack.Push("second");
        //    myStack.Push("third");
        //    Assert.IsFalse(myStack.IsEmpty);
        //    Assert.AreEqual(3, myStack.Count);
        //}

        //[TestMethod]
        //public void topStack()
        //{
        //    Lab2Lib.Stack<string> myStack = new Lab2Lib.Stack<string>();
        //    myStack.Push("first");
        //    myStack.Push("second");
        //    myStack.Push("third");
        //    Assert.AreEqual("third", myStack.Top());
        //}

        //[TestMethod]
        //public void popStack()
        //{
        //    Lab2Lib.Stack<string> myStack = new Lab2Lib.Stack<string>();
        //    myStack.Push("first");
        //    myStack.Push("second");
        //    myStack.Push("third");
        //    Assert.AreEqual("third", myStack.Pop());
        //    Assert.AreEqual("second", myStack.Top());
        //    Assert.AreEqual("second", myStack.Pop());
        //    Assert.AreEqual("first", myStack.Pop());
        //}


        //[TestMethod]
        //public void createAQueue()
        //{
        //    Lab2Lib.Queue<string> myQueue = new Lab2Lib.Queue<string>();
        //    Assert.IsTrue(myQueue.IsEmpty);
        //}

        //[TestMethod]
        //public void addToQueue()
        //{
        //    Lab2Lib.Queue<string> myQueue = new Lab2Lib.Queue<string>();
        //    myQueue.Enqueue("first");
        //    myQueue.Enqueue("second");
        //    myQueue.Enqueue("third");
        //    Assert.IsFalse(myQueue.IsEmpty);
        //    Assert.AreEqual(3, myQueue.Count);
        //}

        //[TestMethod]
        //public void removeFromQueue()
        //{
        //    Lab2Lib.Queue<string> myQueue = new Lab2Lib.Queue<string>();
        //    myQueue.Enqueue("firstIn");
        //    myQueue.Enqueue("secondIn");
        //    myQueue.Enqueue("lastIn");

        //    string firstOut = myQueue.Dequeue();
        //    string secondOut = myQueue.Dequeue();
        //    string lastOut = myQueue.Dequeue();

        //    Assert.AreEqual("firstIn", firstOut);
        //    Assert.AreEqual("secondIn", secondOut);
        //    Assert.AreEqual("lastIn", lastOut);
        //}

        //[TestMethod]
        //public void createExpressionParser()
        //{
        //    Parser parser = new Parser();
        //    Assert.AreEqual(0.0, parser.Evaluation);
        //}

        //[TestMethod]
        //public void tokenizeExpression()
        //{
        //    Parser parser = new Parser();
        //    string[] tokens = parser.Tokenize("( ( 10 + 12 ) * 2312 )");
        //    Assert.AreEqual("(", tokens[0]);
        //    Assert.AreEqual("(", tokens[1]);
        //    Assert.AreEqual("10", tokens[2]);
        //    Assert.AreEqual("+", tokens[3]);
        //    Assert.AreEqual("12", tokens[4]);
        //    Assert.AreEqual(")", tokens[5]);
        //    Assert.AreEqual("*", tokens[6]);
        //    Assert.AreEqual("2312", tokens[7]);
        //    Assert.AreEqual(")", tokens[8]);
        //}

        //[TestMethod]
        //public void canParse()
        //{
        //    Parser parser = new Parser();
        //    Assert.IsTrue(parser.CanParse("( ( 10 + 12 ) * 2312 )"));
        //    Assert.IsFalse(parser.CanParse("( ( 10 + 12 ) * 2312 ) )"));
        //}


        //[TestMethod]
        //public void parse()
        //{
        //    Parser parser = new Parser();
        //    parser.Parse("( 1 + 1 )");
        //    Assert.AreEqual(2.0, parser.Evaluation);
        //}

        //[TestMethod]
        //public void readSimpleExpression()
        //{
        //    Lab2Lib.Queue<string> expressionQueue =
        //        new Lab2Lib.Queue<string>();

        //    Lab2Lib.Queue<double> resultQueue =
        //        new Lab2Lib.Queue<double>();

        //    expressionQueue.Enqueue("( 1 + 1 )");
        //    resultQueue.Enqueue(2.0);
        //    Parser parser = new Parser();

        //    while (!expressionQueue.IsEmpty)
        //    {
        //        parser.Parse(expressionQueue.Dequeue());
        //        Assert.AreEqual(resultQueue.Dequeue(), parser.Evaluation);
        //    }
        //}

        //[TestMethod]
        //public void readExpression()
        //{
        //    Lab2Lib.Queue<string> expressionQueue =
        //        new Lab2Lib.Queue<string>();

        //    Lab2Lib.Queue<double> resultQueue =
        //        new Lab2Lib.Queue<double>();

        //    expressionQueue.Enqueue("( 1 + 1 )");
        //    resultQueue.Enqueue(2.0);
        //    expressionQueue.Enqueue("( 1 + ( ( 2 + 3 ) * ( 4 * 5 ) ) )");
        //    resultQueue.Enqueue(101.0);
        //    expressionQueue.Enqueue("+ )");
        //    resultQueue.Enqueue(103.0);
        //    Parser parser = new Parser();

        //    while (!expressionQueue.IsEmpty)
        //    {
        //        string expression = expressionQueue.Dequeue();
        //        Assert.IsTrue(parser.CanParse(expression));
        //        parser.Parse(expression);
        //        Assert.AreEqual(resultQueue.Dequeue(), parser.Evaluation);
        //    }
        //}
    }
}
