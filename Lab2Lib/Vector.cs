﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2Lib
{
    public class Vector<Type>
    {
        public Vector()
        {
            theVector = new List<Type>();
        }
        public bool IsEmpty
        {
            get { return theVector.Count == 0; }
        }

        List<Type> theVector;
    }
}
